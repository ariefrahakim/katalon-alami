import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WebAPI
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

def response = WS.sendRequestAndVerify(findTestObject('Auth/CIAM - LoginBackOffice', [('ipCiam') : 'https://dev.ciam.hijra.id/api/v1']))

def slurper = new groovy.json.JsonSlurper()

def result = slurper.parseText(response.getResponseBodyContent())

def accessTokens = result.data.accessToken

println('... Value extracted is : ' + accessTokens)

GlobalVariable.access_token = accessTokens

println('... TokenLogin is : ' + GlobalVariable.access_token)

WS.sendRequestAndVerify(findTestObject('API/Auth/CIAM - LogoutBackOffice', [('ipCiam') : 'https://dev.ciam.hijra.id/api/v1'
            , ('access_token') : GlobalVariable.access_token]))

