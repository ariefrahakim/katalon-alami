package hijra

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


import java.text.SimpleDateFormat
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class hijraLogin {

	//	def date = new Date()
	//	def sdf = new SimpleDateFormat('HH-mm-ss.SSS')
	//	String today = sdf.format(date)

	@Given("user go to login page hijra")
	def user_go_to_login_page_hijra() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://alami-dev.dot.co.id/login')
	}

	@When("user input username")
	def user_input_username() {
		WebUI.setText(findTestObject('Object Repository/Hijra-LoginPage/inputUsername'), 'rojak@gmail.com')
		WebUI.delay(1)
	}

	@Then("user input password")
	def user_input_password() {
		WebUI.setText(findTestObject('Object Repository/Hijra-LoginPage/inputPassword'), 'batubata')
		WebUI.delay(1)
		//		WebUI.takeScreenshot('D:/Arief/Capture Katalon/login'+today+'.png')
	}

	@And("user klik login")
	def user_klik_login() {
		WebUI.click(findTestObject('Object Repository/Hijra-LoginPage/buttonLogin'))
		WebUI.delay(1)
		//		WebUI.verifyElementVisible(findTestObject('Object Repository/Page../landingPage'))
		//		WebUI.takeScreenshot('D:/Arief/Capture Katalon/landingPage'+today+'.png')
		WebUI.closeBrowser()
	}
}